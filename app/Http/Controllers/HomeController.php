<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Url;
use Exception;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dup = 0;
        $urls = Url::all();
        $total_no = $urls->count();
        $duplicates = Url::select(DB::raw('count(*) as count, url'))
                    ->groupBy('url')
                    ->get();
        foreach ($duplicates as $raw) {
            if ($raw->count > 1) {
                $dup = $dup + $raw->count;
            }
        }
        return view('home', compact('dup', 'total_no'));
    }

    public function urlList()
    {
        return view('url-list');
    }

    public function displayUrls()
    {
        $table = [];
        $datas = Url::orderBy('id', 'DESC')->get();
        foreach ($datas as $data) {
            $created = explode(' ', $data->created_at);
            $table[] = '<tr>';
            $table[] = '<td>'.$data->title.'</td><td>'.$data->url.'</td><td>'.route('shorten.link', $data['short_url']).'</td><td>'.$created[0].'</td><td><button data-val="'.route('shorten.link', $data['short_url']).'" id="'.$data->id.'" class="copy_url btn-primary">Copy</button></td>';
            $table[] = '</tr>';
        }
        
        return $table;
    }

    public function generateShortUrl(Request $request)
    {
        $request->validate([
            'link' => 'required|url',
            'title' => 'required',
        ]);
        
        $input = new Url;
        $input->title = $request->title;
        $input->url = $request->link;
        $input->short_url = str_random(6);
        $input->save();
        $urls = Url::orderBy('id', 'DESC')->get();
        foreach ($urls as $row) {
            $created = explode(' ', $row->created_at);
            $table[] = '<tr>';
            $table[] = '<td>'.$row['title'].'</td><td>'.$row['url'].'</td><td>'.route('shorten.link', $row['short_url']).'</td><td>'.$created[0].'</td><td><button data-val="'.route('shorten.link', $row['short_url']).'" id="'.$row->id.'" class="copy_url btn-primary">Copy</button></td>';
            $table[] = '</tr>';
        }
        return $table;
    }

    public function shortenLink($code)
    {
        $find = Url::where('short_url', $code)->first();
   
        return redirect($find->url);
    }
}
