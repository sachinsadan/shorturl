<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('url/list', 'HomeController@urlList')->name('url.list');
Route::get('display/urls', 'HomeController@displayUrls')->name('display.urls');
Route::get('generate/short-url', 'HomeController@generateShortUrl')->name('generate.short-url');
Route::get('get/short-url', 'HomeController@getShortUrl')->name('get.short-url');
Route::get('{code}', 'HomeController@shortenLink')->name('shorten.link');
