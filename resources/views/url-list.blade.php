@extends('layouts.dash')

@section('content')
<div class="card">
    <div class="url-div">
        <div class="row" style="margin-top: 15px; padding-right: 10px; margin-bottom: 15px;">
            <div class="col-md-4">
                <input type="text" id="url-link" class="url_data form-control m-input" placeholder="Enter Url">
            </div>
            <div class="col-md-3">
                <input type="text" id="title" class="url_data form-control m-input" placeholder="Enter title">
            </div>
            <div class="col-md-3">
            <button class="btn btn-primary" id="shorten-url"> Shorten</button>
            </div>
        </div>
    </div>
    <table class="table table-bordered table-sm">
        <thead>
            <tr>
                <th>Title</th>
                <th>URL</th>
                <th>Short URL</th>
                <th>Created Date</th>
                <th>Copy URL</th>
            </tr>
        </thead>
        <tbody id="tbl-body">
            
        </tbody>
    </table>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: '{{ route("display.urls") }}',
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success:function(data) {
                console.log(data);
                $('#tbl-body').html(data);
            }
        });
    });

    // create short url
    $('#shorten-url').click(function(e) {
        e.preventDefault();
        var link = $('#url-link').val();
        var title = $('#title').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: '{{ route("generate.short-url") }}',
            data: {
                "_token": "{{ csrf_token() }}",
                "link": link,
                "title": title,
            },
            success:function(data) {
                $('.url_data').val('');
                $('#tbl-body').empty();
                $('#tbl-body').append(data);
            }
        });
    })

    // copy url
    $(document).on('click', '.copy_url', function(e) {
        var short_url = $(this).attr('data-val');
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(short_url).select();
        document.execCommand("copy");
        $temp.remove();
    })
</script>
@endpush